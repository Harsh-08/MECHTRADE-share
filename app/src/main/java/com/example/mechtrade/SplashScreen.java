package com.example.mechtrade;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mechtrade.allactivities.Admin;
import com.example.mechtrade.allactivities.allcomponents.AdminLoginPage;
import com.example.mechtrade.allactivities.allcomponents.ManagerLogin;
import com.example.mechtrade.allactivities.allcomponents.UserLogin;
import com.imangazaliev.circlemenu.CircleMenu;
import com.imangazaliev.circlemenu.CircleMenuText;

import java.util.List;


public class SplashScreen extends AppCompatActivity {



    // TODO: 11/5/2019 make it circular menu

    Button buttonadmin, buttonmng , buttonuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        buttonadmin = (Button)findViewById(R.id.home_button_admin);
        buttonadmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intenta = new Intent(SplashScreen.this, AdminLoginPage.class);
                startActivity(intenta);
            }
        });

        buttonmng = (Button)findViewById(R.id.home_button_mng);
        buttonmng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(SplashScreen.this, ManagerLogin.class);
                startActivity(myIntent);
            }
        });

        buttonuser = (Button)findViewById(R.id.home_button_user);
        buttonuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntentu = new Intent(SplashScreen.this, UserLogin.class);
                startActivity(myIntentu);
            }
        });
    }
}
