package com.example.mechtrade.allactivities.allcomponents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mechtrade.R;
import com.example.mechtrade.allactivities.Admin;

public class AdminLoginPage extends AppCompatActivity {
    Button adminsigninButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login_page);

        adminsigninButton = (Button)findViewById(R.id.loginadmin);
        adminsigninButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(AdminLoginPage.this, Admin.class);
                startActivity(loginIntent);
            }
        });


    }
}
