package com.example.mechtrade.allactivities.allcomponents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mechtrade.R;
import com.example.mechtrade.allactivities.Admin;
import com.example.mechtrade.allactivities.User;

public class UserLogin extends AppCompatActivity {

    Button usersigninButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        usersigninButton = (Button)findViewById(R.id.loginuser);
        usersigninButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(UserLogin.this, User.class);
                startActivity(loginIntent);
            }
        });
    }
}
