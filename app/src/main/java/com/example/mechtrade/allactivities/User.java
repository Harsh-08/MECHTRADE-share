package com.example.mechtrade.allactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mechtrade.R;
import com.example.mechtrade.allactivities.allcomponents.AssignedJob;
import com.example.mechtrade.allactivities.allcomponents.AssignedSide;
import com.example.mechtrade.allactivities.allcomponents.AssignedTools;
import com.example.mechtrade.allactivities.allcomponents.Checklist;
import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;

public class User extends AppCompatActivity {

    // TODO: 11/5/2019 Assigned site   
    // TODO: 11/5/2019 Assigned job 
    // TODO: 11/5/2019 your check list
    // TODO: 11/5/2019 readiness report
    
    String arrayName[]={"Assigned Site", "Assigned Job", "Assigned Tools" , "Readiness Report", "Check List"};

    CircleMenu circleMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        circleMenu = (CircleMenu)findViewById(R.id.circle_menu);
        circleMenu.setMainMenu(Color.parseColor("#ff6b1c"),R.drawable.heylogonew,R.drawable.remove)
                .addSubMenu(Color.parseColor("#362573"),R.drawable.hospital)
                .addSubMenu(Color.parseColor("#30942e"),R.drawable.joblogo)
                .addSubMenu(Color.parseColor("#b52b2b"),R.drawable.customersupport)
                .addSubMenu(Color.parseColor("#32d9c2"),R.drawable.report)

                .setOnMenuSelectedListener(new OnMenuSelectedListener() {
                    @Override
                    public void onMenuSelected(int index) {
                        switch (index){
                            case 0:
                                Intent intent = new Intent(User.this, AssignedSide.class);
                                startActivity( intent);
                                break;

                            case 1:
                                Intent newIntent1 = new Intent(User.this, AssignedJob.class);
                                startActivity(newIntent1);
                                break;

                            case 2:
                                Intent intent1 = new Intent(User.this, AssignedTools.class);
                                startActivity(intent1);
                                break;

                            case 3:
                                Intent intent2 = new Intent(User.this, Checklist.class);
                                startActivity(intent2);
                                break;
                        }
                        Toast.makeText(User.this, "Opening  " +arrayName[index], Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
