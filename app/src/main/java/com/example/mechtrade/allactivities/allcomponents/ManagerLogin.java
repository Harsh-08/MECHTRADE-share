package com.example.mechtrade.allactivities.allcomponents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mechtrade.R;
import com.example.mechtrade.allactivities.Admin;
import com.example.mechtrade.allactivities.Manager;

public class ManagerLogin extends AppCompatActivity {

    Button mngsigninButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_login);

        mngsigninButton = (Button)findViewById(R.id.loginmng);
        mngsigninButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(ManagerLogin.this, Admin.class);
                startActivity(loginIntent);
            }
        });

    }
}
