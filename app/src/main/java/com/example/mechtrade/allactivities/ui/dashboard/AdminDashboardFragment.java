package com.example.mechtrade.allactivities.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.mechtrade.allactivities.allcomponents.CallendarLayout;
import com.example.mechtrade.R;
import com.example.mechtrade.allactivities.allcomponents.LastSevnDayActivity;
import com.example.mechtrade.allactivities.allcomponents.ManagaTools;
import com.example.mechtrade.allactivities.allcomponents.ManageToolShifting;
import com.example.mechtrade.allactivities.allcomponents.ManageUsers;


public class AdminDashboardFragment extends Fragment {
    ImageButton imageButton , imgjob , callender , team , tools , shift;

    private AdminDashboardViewModel dashboardViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        dashboardViewModel = ViewModelProviders.of(this).get(AdminDashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final TextView textView = root.findViewById(R.id.textdashboard);
        dashboardViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });


        imageButton = (ImageButton) root.findViewById(R.id.img1);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LastSevnDayActivity.class);
                startActivity(intent);
            }
        });


        imgjob = (ImageButton) root.findViewById(R.id.job);
        imgjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getActivity(), LastSevnDayActivity.class);
                startActivity(newIntent);
            }
        });

        callender = (ImageButton) root.findViewById(R.id.cal_lay);
        callender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), CallendarLayout.class);
                startActivity(myIntent);
            }
        });


        team = (ImageButton) root.findViewById(R.id.teammng);
        team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(getActivity(), ManageUsers.class);
                startActivity(mIntent);
            }
        });

        tools = (ImageButton) root.findViewById(R.id.toolsmng);
        tools.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mnewIntent = new Intent(getActivity(), ManagaTools.class);
                startActivity(mnewIntent);
            }
        });

        shift = (ImageButton) root.findViewById(R.id.shift);
        shift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mwIntent = new Intent(getActivity(), ManageToolShifting.class);
                startActivity(mwIntent);
            }
        });


        return root;
    }
}


//     SlideshowFragment slideshowFragment = new SlideshowFragment();
//                FragmentManager manager = getFragmentManager();
//                manager.beginTransaction()
//                        .replace(R.id.home_layout, slideshowFragment ,slideshowFragment.getTag())
//                        .commit();
//            }